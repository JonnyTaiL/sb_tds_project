// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SB_TopDownShooter : ModuleRules
{
	public SB_TopDownShooter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
