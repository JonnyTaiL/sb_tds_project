// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SB_TopDownShooterGameMode.generated.h"

UCLASS(minimalapi)
class ASB_TopDownShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASB_TopDownShooterGameMode();
};



