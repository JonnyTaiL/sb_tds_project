// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_TopDownShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SB_TopDownShooter, "SB_TopDownShooter" );

DEFINE_LOG_CATEGORY(LogSB_TopDownShooter)
 